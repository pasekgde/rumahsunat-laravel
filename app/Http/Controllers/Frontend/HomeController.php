<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use Artesaos\SEOTools\Facades\SEOTools;
use Artesaos\SEOTools\Facades\SEOMeta;
use Artesaos\SEOTools\Facades\OpenGraph;
use Artesaos\SEOTools\Facades\TwitterCard;
use Artesaos\SEOTools\Facades\JsonLd;


/**
 * Class HomeController.
 */
class HomeController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */


    public function index()
    {

    	$title = 'Homepage - Rumah Sunat Bali';
        $description = 'Rumah Sunat Bali - Layanan Sunat Anak, Remaja, & Dewasa';
        $url = 'http://www.rumahsunatbali.com';
        $image = 'https://codecasts.com.br/img/logo.jpg';

        SEOMeta::setTitle($title,false);
        SEOMeta::setDescription($description);
        SEOMeta::setCanonical($url);

        OpenGraph::setDescription($description);
        OpenGraph::setTitle($title);
        OpenGraph::setUrl($url);
        OpenGraph::addProperty('type', 'homepage');

        TwitterCard::setTitle($title);
        TwitterCard::setSite('@rumahsunatbali');

        JsonLd::setTitle($title);
        JsonLd::setDescription($description);
        JsonLd::addImage($image);

    	$blogsdata = DB::table('blog_entries')->orderBy('id', 'desc')->paginate(6);
        return view('frontend.index', ['blogsdata'=>$blogsdata] );
        //return view('frontend.index');
    }

    
}
