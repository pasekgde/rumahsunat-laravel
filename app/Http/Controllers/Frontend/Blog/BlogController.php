<?php

namespace App\Http\Controllers\Frontend\Blog;

use App\Http\Controllers\Controller;
use App\Repositories\Frontend\Blog\BlogRepository;
use Illuminate\Support\Facades\DB;
use Artesaos\SEOTools\Facades\SEOTools;
use Artesaos\SEOTools\Facades\SEOMeta;
use Artesaos\SEOTools\Facades\OpenGraph;
use Artesaos\SEOTools\Facades\TwitterCard;
use Artesaos\SEOTools\Facades\JsonLd;
/**
 * Class HomeController.
 */
class BlogController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */

      protected $blogentry;

    public function __construct(BlogRepository $blogentry)
    {
        //Instance repository UserRepository kedalam property $user
        $this->blogentry = $blogentry;
    }


    public function all()
    {
    	return $this->blogentry->showall();
        // return view('frontend.blogs', ['blogsdata'=>$blogsdata] );
    }   

    public function seoblog()
    {
        $title = 'Blog Artikel - Rumah Sunat Bali';
        $description = 'Rumah Sunat Bali - Layanan Sunat Anak, Remaja, & Dewasa';
        $url = url('/').'/blogs';
        $image = url('/frontend/assets/images/logo.png');

        SEOMeta::setTitle($title,false);
        SEOMeta::setDescription($description);
        SEOMeta::setCanonical($url);

        OpenGraph::setDescription($description);
        OpenGraph::setTitle($title);
        OpenGraph::setUrl($url);
        OpenGraph::addProperty('type', 'homepage');

        TwitterCard::setTitle($title);
        TwitterCard::setSite('@rumahsunatbali');

        JsonLd::setTitle($title);
        JsonLd::setDescription($description);
        JsonLd::addImage($image);
    }

    public function index()
    {
        $this->seoblog();

        $alldata = DB::table('blog_entries')->orderBy('id', 'desc')->paginate(3);
        $blogsdata = DB::table('blog_entries')->orderBy('id', 'desc')->paginate(5);
        return view('frontend.blogs', ['blogsdata'=>$blogsdata],['alldata'=>$alldata] );
    }





    public function article($slug)
    {
        
    	$blogsdata = $this->blogentry->find($slug)->first();   

        $title = $blogsdata->title;
        $description = $blogsdata->summary;
        $url = url('/').'blog/'.$blogsdata->slug;
        $image = url('/').$blogsdata->image;
        $type = 'Blog.Article';

        SEOMeta::setTitle($title,false);
        SEOMeta::setDescription($description);
        SEOMeta::setCanonical($url);

        OpenGraph::setDescription($description);
        OpenGraph::setTitle($title);
        OpenGraph::setUrl($url);
        OpenGraph::addProperty('type', $type);

        TwitterCard::setTitle($title);
        TwitterCard::setSite('@rumahsunatbali');

        JsonLd::setTitle($title);
        JsonLd::setDescription($description);
        JsonLd::addImage($image);


        $alldata = DB::table('blog_entries')->orderBy('id', 'desc')->paginate(3);

        return view('frontend.article', ['blogsdata'=>$blogsdata], ['alldata'=>$alldata] );
        
    }
}
