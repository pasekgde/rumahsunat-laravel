
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import '../../bootstrap';
import '../../plugins';
import Vue from 'vue';

window.Vue = Vue;

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('table-workingpermit', require('./components/TableWorkingPermitComponent.vue').default);
Vue.component('form-workingpermit', require('./components/FormWorkingPermitComponent.vue').default);
Vue.component('pagination', require('./components/PaginationComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

 //dynamic url
let myUrl = window.location.origin+"/";

if(myUrl.includes("localhost")){
  var pathArray = window.location.pathname.split( '/' );
  myUrl += pathArray [1] + "/";
}

const app = new Vue({
    el: '#app',
    data: {
        url: myUrl,
         editModal:false,
                    addModal:false,
                    deleteModal:false,
        choosedWorkingPermit:{},
        showFormWorkingPermit:false,
        showTableWorkingPermit:true,
        renderComponent: true,
        openTab: true,
    },
    methods: {// receives a place object via the autocomplete component
                
        getDataWorkingPermit(value){
            if(value.action==='edit'){
                this.showFormWorkingPermit=true
                this.showTableWorkingPermit=false
                this.choosedWorkingPermit = value.dataWorkingPermit
            }else{
                this.$refs.tableWorkingPermit.showAll()
            }
            
        },
        addNewWorkingPermit(){
           this.showFormWorkingPermit=true
           this.showTableWorkingPermit=false 
        },
        isHideForm(value){
            console.log(value)
            this.showFormWorkingPermit=value.isShowForm
            this.showTableWorkingPermit=!value.isShowForm
        }







    }
});