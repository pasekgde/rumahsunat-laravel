    <section class="row top_bar">
        <div class="container">
            <div class="row m0">
                <div class="fleft schedule"><strong><i class="fa fa-clock-o"></i> Schedule</strong>: Everyday - 9:00 - 20:00</div>
                <div class="fright contact_info">
                    <div class="fleft email"><img src="{{ url('frontend/')}}/images/icons/envelope.jpg" alt=""> info@rumahsunatbali.com</div>
                    <div class="fleft phone"><i class="fa fa-phone"></i> <strong>(0877) 7711 4800</strong></div>
                </div>
            </div>
        </div>
    </section>