@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')
    
    <!--Main Slider-->
<section id="home">
    <section class="main-slider" data-start-height="800" data-slide-overlay="yes">
        
        <div class="tp-banner-container">
            <div class="tp-banner">
                <ul>
                    
                    <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb="{{ url('frontend/assets/')}}/images/main-slider/image-1.jpg" data-saveperformance="off" data-title="Awesome Title Here">
                    <img src="{{ url('frontend/assets/')}}/images/main-slider/image-1.jpg" alt="" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 
                    
                    <!-- <div class="overlay-slide"></div>
                    
                    <div class="tp-caption sft sfb tp-resizeme" data-x="center" data-hoffset="0" data-y="center" data-voffset="-180" data-speed="1500" data-start="0" data-easing="easeOutExpo" data-splitin="none" data-splitout="none" data-elementdelay="0.01" data-endelementdelay="0.3" data-endspeed="1200" data-endeasing="Power4.easeIn"><figure class="content-image"><img src="{{ url('frontend/assets/')}}/images/main-slider/content-image-1.png" alt=""></figure> </div>
                    
                    <div class="tp-caption sft sfb tp-resizeme" data-x="center" data-hoffset="0" data-y="center" data-voffset="-70" data-speed="1500" data-start="500" data-easing="easeOutExpo" data-splitin="none" data-splitout="none" data-elementdelay="0.01" data-endelementdelay="0.3" data-endspeed="1200" data-endeasing="Power4.easeIn"><div class="styled-text">welcome to our driving school</div></div>
                    
                    <div class="tp-caption sfb sfb tp-resizeme" data-x="center" data-hoffset="0" data-y="center" data-voffset="25" data-speed="1500" data-start="1000" data-easing="easeOutExpo" data-splitin="none" data-splitout="none" data-elementdelay="0.01" data-endelementdelay="0.3" data-endspeed="1200" data-endeasing="Power4.easeIn"><div class="border-heading">Your Childrens Are Safe With us</div></div>
                    
                    <div class="tp-caption sfb sfb tp-resizeme" data-x="center" data-hoffset="0" data-y="center" data-voffset="115" data-speed="1500" data-start="1500" data-easing="easeOutExpo" data-splitin="none" data-splitout="none" data-elementdelay="0.01" data-endelementdelay="0.3" data-endspeed="1200" data-endeasing="Power4.easeIn"><div class="text">See Our Kindergarten Special Features!</div></div>
                    
                    <div class="tp-caption sfb sfb tp-resizeme" data-x="center" data-hoffset="0" data-y="center" data-voffset="175" data-speed="1500" data-start="2000" data-easing="easeOutExpo" data-splitin="none" data-splitout="none" data-elementdelay="0.01" data-endelementdelay="0.3" data-endspeed="1200" data-endeasing="Power4.easeIn"><a href="#" class="theme-btn btn-style-one">Let’s Go</a></div> -->
                    
                    </li>
                    
                    <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb="{{ url('frontend/assets/')}}/images/main-slider/image-4.jpg" data-saveperformance="off" data-title="Awesome Title Here">
                    <img src="{{ url('frontend/assets/')}}/images/main-slider/image-4.jpg" alt="" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 
                    
                    <!-- <div class="tp-caption sfl sfb tp-resizeme" data-x="left" data-hoffset="450" data-y="center" data-voffset="-140" data-speed="1500" data-start="500" data-easing="easeOutExpo" data-splitin="none" data-splitout="none" data-elementdelay="0.01" data-endelementdelay="0.3" data-endspeed="1200" data-endeasing="Power4.easeIn"><h4>Layanan Fokus</h4></div>
                    
                    <div class="tp-caption sfl sfb tp-resizeme" data-x="left" data-hoffset="450" data-y="center" data-voffset="-40" data-speed="1500" data-start="1000" data-easing="easeOutExpo" data-splitin="none" data-splitout="none" data-elementdelay="0.01" data-endelementdelay="0.3" data-endspeed="1200" data-endeasing="Power4.easeIn"><h2>SIRKUMSISI</h2></div>
                    
                    <div class="tp-caption sfl sfb tp-resizeme" data-x="left" data-hoffset="450" data-y="center" data-voffset="70" data-speed="1500" data-start="1500" data-easing="easeOutExpo" data-splitin="none" data-splitout="none" data-elementdelay="0.01" data-endelementdelay="0.3" data-endspeed="1200" data-endeasing="Power4.easeIn"><div class="dark-text">Layanan kesehatan yang fokus dalam Sirkumsisi/khitan laki-laki dengan metode yang profesional</div></div>
                    
                    <div class="tp-caption sfl sfb tp-resizeme" data-x="left" data-hoffset="450" data-y="center" data-voffset="150" data-speed="1500" data-start="2000" data-easing="easeOutExpo" data-splitin="none" data-splitout="none" data-elementdelay="0.01" data-endelementdelay="0.3" data-endspeed="1200" data-endeasing="Power4.easeIn"><a href="#" class="theme-btn btn-style-one">Let’s Go</a>&ensp; &ensp;<a href="#" class="theme-btn btn-style-two">Read more </a></div>  -->                   
                    </li>

                    <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb="{{ url('frontend/assets/')}}/images/main-slider/image-5.jpg" data-saveperformance="off" data-title="Awesome Title Here">
                    <img src="{{ url('frontend/assets/')}}/images/main-slider/image-5.jpg" alt="" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 
                    
                    <!-- <div class="tp-caption sfl sfb tp-resizeme" data-x="left" data-hoffset="450" data-y="center" data-voffset="-140" data-speed="1500" data-start="500" data-easing="easeOutExpo" data-splitin="none" data-splitout="none" data-elementdelay="0.01" data-endelementdelay="0.3" data-endspeed="1200" data-endeasing="Power4.easeIn"><h4>Layanan Fokus</h4></div>
                    
                    <div class="tp-caption sfl sfb tp-resizeme" data-x="left" data-hoffset="450" data-y="center" data-voffset="-40" data-speed="1500" data-start="1000" data-easing="easeOutExpo" data-splitin="none" data-splitout="none" data-elementdelay="0.01" data-endelementdelay="0.3" data-endspeed="1200" data-endeasing="Power4.easeIn"><h2>SIRKUMSISI</h2></div>
                    
                    <div class="tp-caption sfl sfb tp-resizeme" data-x="left" data-hoffset="450" data-y="center" data-voffset="70" data-speed="1500" data-start="1500" data-easing="easeOutExpo" data-splitin="none" data-splitout="none" data-elementdelay="0.01" data-endelementdelay="0.3" data-endspeed="1200" data-endeasing="Power4.easeIn"><div class="dark-text">Layanan kesehatan yang fokus dalam Sirkumsisi/khitan laki-laki dengan metode yang profesional</div></div>
                    
                    <div class="tp-caption sfl sfb tp-resizeme" data-x="left" data-hoffset="450" data-y="center" data-voffset="150" data-speed="1500" data-start="2000" data-easing="easeOutExpo" data-splitin="none" data-splitout="none" data-elementdelay="0.01" data-endelementdelay="0.3" data-endspeed="1200" data-endeasing="Power4.easeIn"><a href="#" class="theme-btn btn-style-one">Let’s Go</a>&ensp; &ensp;<a href="#" class="theme-btn btn-style-two">Read more </a></div>                     -->
                    </li>
                    
                </ul>
                
            </div>
        </div>
        
    </section>
</section>
    <!--End Main Slider-->

<section class="featured-section style-two">
        <div class="auto-container">
            <!--Sec Title-->
            <div class="sec-title centered">
                <div class="title-icon"><img src="{{ url('frontend/assets/')}}/images\icons\sec-title-icon-1.png" alt=""></div>
                <h2>Selamat datang di Rumah Sunat Bali</h2>
                <div class="title">Welcome to Rumah Sunat Bali</div>
                <div class="text">Layanan kesehatan rawat jalan modern berbasis  dokter praktek khusus untuk sunat / sirkumsisi . pelayanan dengan standarisasi yang tinggi dan dikerjakan oleh dokter yang berpengalaman dalam tindakan sirkumsisi dari usia bayi hinga dewasa.  (Modern medical outpatient services based on general practitioner practice for circumcision. Our services are highly standardized and managed by doctors who are experienced in circumcision for babies to adults).</div>
            </div>
            <!--Image-->
            <figure class="image">
               <center><img src="{{ url('frontend/assets/')}}/images\resource\welcome-img.png" alt=""></center>
            </figure>
        </div>
        <div class="footer-style-two">
            <div class="footer-bg"></div>
        </div>
    </section>



    <!--Featured Section-->
<section id="metode">
    <section class="featured-section style-two">
        <div class="auto-container">
            <!--Sec Title-->
            <div class="sec-title centered">
                <div class="title-icon"><img src="{{ url('frontend/assets/')}}/images\icons\sec-title-icon-1.png" alt=""></div>
                <h2>METODE SUNAT KAMI</h2>
                <div class="title">Our Circumcision Methods </div>
                <div class="text">Rumah Sunat Bali menyediakan berbagai pilihan metode dalam sirkumsisi sesuai dengan kebutuhan dan pemeriksaan oleh dokter kami, untuk sunat mulai usia bayi hingga dewasa”. (Rumah Sunat Bali provides a wide selection of methods in circumcision according to the needs and examinations by our doctors, from babies to adults)</div>
                
            </div>
            <!--End Sec Title-->
            <div class="row clearfix">
                <!--Column/ Pull Left-->
                <div class="column pull-left col-md-4 col-sm-6 col-xs-12">
                    <!--Feature Block-->
                    <div class="feature-block">
                        <div class="inner-box">
                            <div class="icon-box">
                                <img src="{{ url('frontend/assets/')}}/images\metode\konvensional.png" alt="">
                            </div>
                            <h3><a>SUNAT KONVENSIONAL / CAUTERY</a></h3>
                            <div class="text">Sunat dengan metode bedah minor, dengan luka jahitan konvensional. (Circumcision by minor surgical methods, with conventional suture wounds)</div>
                        </div>
                    </div>
                    
                    <!--Feature Block-->
                    <div class="feature-block">
                        <div class="inner-box">
                            <div class="icon-box">
                                <img src="{{ url('frontend/assets/')}}/images\metode\klam.png" alt="">
                            </div>
                            <h3><a>SUNAT CLAMP / KLEM</a></h3>
                            <div class="text">Sunat dengan metode clamp , menggunakan device khusus yang mencetak hasil sunat sehingga hasil rapi sesuai device clamp yang digunakan. Metode ini tanpa menggunakan jahitan kulit pada luka.  
                            (Circumcision with the clamp method, using a special device that prints the results of the circumcision’s cut. the wound results are neat according to the clamp device used. This method does not use stitches on the wound)</div>
                        </div>
                    </div>
                    
                </div>
                <!--Column / Pull Right-->
                <div class="column pull-right col-md-4 col-sm-6 col-xs-12">
                    <!--Feature Block Two-->
                    <div class="feature-block-two">
                        <div class="inner-box">
                            <div class="icon-box">
                                <img src="{{ url('frontend/assets/')}}/images\metode\stapler.png" alt="">
                            </div>
                            <h3><a>SUNAT METODE BIPOLAR CAUTERY </a></h3>
                            <div class="text">Sunat dengan device bipolar cautery , penyempurnaan metode konvensional dengan hasil yang lebih aestetis dan minim resiko perdarahan. (Circumcision with bipolar cautery device, a refinement of conventional methods with more aesthetic results and minimal risk of bleeding)</div>
                        </div>
                    </div>
                    
                    
                    <!--Feature Block Two-->
                    <div class="feature-block-two">
                        <div class="inner-box">
                            <div class="icon-box">
                                <img src="{{ url('frontend/assets/')}}/images\metode\ring.png" alt="">
                            </div>
                            <h3><a>SUNAT STAPLER</a></h3>
                            <div class="text">Sunat dengan metode stapler, mengadopsi teknik konvensional dank lamp  dengan bantuan device stapler untuk penyatuan kulit, praktis, nyaman dan hasil rapih.  (Circumcision with the stapling method, adopting conventional techniques and clamps with the help of a stapler device for skin integration, practical, comfortable and neat results)</div>
                        </div>
                    </div>
                    
                </div>
            
                <div class="image-column col-md-4 col-sm-12 col-xs-12">
                    <figure class="image wow fadeInUp">
                        <img src="{{ url('frontend/assets/')}}/images\resource\metodekami1.png" alt="">
                    </figure>
                </div>
                
            </div>
            
        </div>
    </section>
</section>
    <!--End Featured Section-->
    
    <!--Join Section-->

    <section class="join-section" style="background-image:url({{ url('frontend/assets/')}}/images/background/pattern-1.png);">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Column-->
                <div class="column col-md-9 col-sm-12 col-xs-12">
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                            <h2>Get Involved!</h2>
                        </div>
                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                            <div class="text">Nothing is more important than your health. Join our seminars and training and learn how to save them.</div>
                        </div>
                    </div>
                </div>
                <!--Column-->
                <div class="btn-column col-md-3 col-sm-12 col-xs-12">
                    <a href="{{ url('contact')}}" class="theme-btn btn-style-one">Join Now</a>
                </div>
            </div>
        </div>
    </section>
    <!--End Join Section-->
<section id="layanan">
    <section class="welcome-section" >
        <div class="auto-container">
            <!--Sec Title-->
            <div class="sec-title centered">
                <figure class="image">
                    <img src="{{ url('frontend/assets/')}}/images\resource\fasilitaskami.png" alt="">
                </figure>
                <!-- <div class="title">Our Weekly Classes</div>
                <div class="text">We are group of teachers who really love childrens and enjoy every moment of teaching</div> -->
            </div>
            <div class="row clearfix">
            
                <!--News Style Two-->
                <div class="news-style-two col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <!--Image Column-->
                        <div class="image-column">
                            <div class="image">
                                <a><img src="{{ url('frontend/assets/')}}/images\pelayanan\BAYI.png" alt=""></a>
                                <div class="overlay-layer">
                                </div>
                            </div>
                        </div>
                        <!--Content Column-->
                        <div class="content-column">
                            <div class="inner">
                                <!-- div class="post-date">Feb 29, 2017</div> -->
                                <h3><a>Sunat Bayi &amp; Anak</a></h3>
                                <div class="text">Sunat pada usia 0 sampai 12 tahun menggunakan Metode Konvensional bedah dengan Guillotine teknik atau metode sunat laser</div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--News Style Two-->
                <div class="news-style-two col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <!--Image Column-->
                        <div class="image-column">
                            <div class="image">
                                <a><img src="{{ url('frontend/assets/')}}/images\pelayanan\REMAJA.png" alt=""></a>
                                <div class="overlay-layer">
                                </div>
                            </div>
                        </div>
                        <!--Content Column-->
                        <div class="content-column">
                            <div class="inner">
                                <!-- <div class="post-date">Marc 17, 2017</div> -->
                                <h3><a>Sunat Remaja</a></h3>
                                <div class="text">Sunat pada usia 12 sampai 17 tahun menggunakan Metode Konvensional Bedah dan Sunat Stapler</div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--News Style Two-->
                <div class="news-style-two col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <!--Image Column-->
                        <div class="image-column">
                            <div class="image">
                                <a><img src="{{ url('frontend/assets/')}}/images\pelayanan\DEWASA.png" alt=""></a>
                                <div class="overlay-layer">
                                </div>
                            </div>
                        </div>
                        <!--Content Column-->
                        <div class="content-column">
                            <div class="inner">
                                <!-- <div class="post-date">Apr 01, 2017</div> -->
                                <h3><a>Sunat Dewasa</a></h3>
                                <div class="text">Sunat Dewasa untuk laki laki diatas umur 17 tahun dengan pilihan Metode Sunat Konvensional (Sunat Laser) dan Sunat Stapler</div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>

            <div class="row clearfix">
            
                <!--News Style Two-->
                <div class="news-style-two col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <!--Image Column-->
                        <div class="image-column">
                            <div class="image">
                                <a><img src="{{ url('frontend/assets/')}}/images\pelayanan\GEMUK.png" alt=""></a>
                                <div class="overlay-layer">
                                </div>
                            </div>
                        </div>
                        <!--Content Column-->
                        <div class="content-column">
                            <div class="inner">
                                <!-- div class="post-date">Feb 29, 2017</div> -->
                                <h3><a>Sunat Gemuk</a></h3>
                                <div class="text">Sunat Gemuk untuk pasien memiliki berat diatas normal usia dan Indeks Masa Tubuh, dengan metode Konvensional dan Clamp (Klem)</div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--News Style Two-->
                <div class="news-style-two col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <!--Image Column-->
                        <div class="image-column">
                            <div class="image">
                                <a><img src="{{ url('frontend/assets/')}}/images\pelayanan\KHUSUS.png" alt=""></a>
                                <div class="overlay-layer">
                                </div>
                            </div>
                        </div>
                        <!--Content Column-->
                        <div class="content-column">
                            <div class="inner">
                                <!-- <div class="post-date">Marc 17, 2017</div> -->
                                <h3><a>Sunat Kondisi Khusus</a></h3>
                                <div class="text">Sunat Kondisi Khusus seperti adanya perbaikan hasil sunat yang tidak rapi, kulup yang masih panjang, pembengkakan kulup atau abnormal</div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--News Style Two-->
                <div class="news-style-two col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <!--Image Column-->
                        <div class="image-column">
                            <div class="image">
                                <a><img src="{{ url('frontend/assets/')}}/images\pelayanan\HOMESERVICE.png" alt=""></a>
                                <div class="overlay-layer">
                                </div>
                            </div>
                        </div>
                        <!--Content Column-->
                        <div class="content-column">
                            <div class="inner">
                                <!-- <div class="post-date">Apr 01, 2017</div> -->
                                <h3><a>Home Care Service</a></h3>
                                <div class="text">Tindakan sunat / rawat luka dirumah dilaksanakan untuk kondisi khusus yang telah dikonfirmasi dokter di rumah sunat bali.</div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
</section>

    <!--Call To Action-->
    <section class="call-to-action" style="background-image:url({{ url('frontend/assets/')}}/images/background/2.jpg);">
        <div class="auto-container">
            <div class="row clearfix">
            
                <!--Content Column-->
                <div class="content-column pull-right col-md-8 col-sm-12 col-xs-12">
                    <h3><span>Yuk Booking, </span>Bagaimana cara Booking <br> Rumah Sunat Bali?</h3>
                    <!-- <div class="text">We are group of teachers who really love childrens and eaching and playing with our students.enjoy every and playing with our students.</div> -->
                    <a href="{{ url('contact')}}" class="theme-btn btn-style-one">Contact Now</a>
                </div>
                <!--Image Column-->
                <div class="image-column pull-left col-md-4 col-sm-12 col-xs-12">
                    <figure class="image wow fadeInUp">
                        <img src="{{ url('frontend/assets/')}}/images\resource\baby.png" alt="">
                    </figure>
                </div>
                
            </div>
            
           
            
        </div>
    </section>
    <!--End Call To Action-->
    
    
    <!--Testimonial Section-->
    <section class="testimonial-section" style="background-image:url({{ url('frontend/assets/')}}/images/background/pattern-1.png);">
        <div class="auto-container">
            
            <!--Client Testimonial Carousel-->
            <div class="client-testimonial-carousel owl-carousel owl-theme">
            
                <!--Testimonial Block Two-->
                <div class="testimonial-block-two">
                    <div class="inner-box">
                        <div class="quote-icon"><span class="icon flaticon-left-quote"></span></div>
                        <div class="text">Ruang Konsultasi Nyaman dan Asik</div>
                    </div>
                </div>
                
                <!--Testimonial Block Two-->
                <div class="testimonial-block-two">
                    <div class="inner-box">
                        <div class="quote-icon"><span class="icon flaticon-left-quote"></span></div>
                        <div class="text">Info dari dokter detail banget dan anak tidak takut</div>
                    </div>
                </div>
                
                <!--Testimonial Block Two-->
                <div class="testimonial-block-two">
                    <div class="inner-box">
                        <div class="quote-icon"><span class="icon flaticon-left-quote"></span></div>
                        <div class="text">Untuk hal yang seumur hidup, pelayanan Rumah Sunat Bali Reccomended </div>
                    </div>
                </div>
                
            </div>
            
            <!--Product Thumbs Carousel-->
            <div class="client-thumb-outer">
                <div class="client-thumbs-carousel owl-carousel owl-theme">
                    <div class="thumb-item">
                        <figure class="thumb-box"><img src="{{ url('frontend/assets/')}}/images\resource\author-1.jpg" alt=""></figure>
                        <div class="thumb-content">
                            <h3>Tahufan Hali</h3>
                            <div class="designation">Entreprenour</div>
                        </div>
                    </div>
                    <div class="thumb-item">
                        <figure class="thumb-box"><img src="{{ url('frontend/assets/')}}/images\resource\author-2.jpg" alt=""></figure>
                        <div class="thumb-content">
                            <h3>Kadek Juli Nugraha</h3>
                            <div class="designation">Swasta</div>
                        </div>
                    </div>
                    <div class="thumb-item">
                        <figure class="thumb-box"><img src="{{ url('frontend/assets/')}}/images\resource\author-3.jpg" alt=""></figure>
                        <div class="thumb-content">
                            <h3>Agustina Indri</h3>
                            <div class="designation">PNS</div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </section>
    <!--End Testimonial Section-->
    
    <!--Blog Section-->
<section id="blog">
    <section class="blog-section">
        <div class="auto-container">
            <!--Sec Title-->
            <div class="sec-title centered">
                <div class="title-icon"><img src="{{ url('frontend/assets/')}}/images\icons\sec-title-icon-1.png" alt=""></div>
                <h2>blog & Activities</h2>
                <br><br><br>
            </div>
             
            <div class="row clearfix">
                <!--Blog Column-->
               @foreach($blogsdata->chunk(3) as $chunk)
                <div class="blog-column col-md-6 col-sm-12 col-xs-12">
                    <!--News Style Three-->
                    @foreach($chunk as $data)
                    <div class="news-style-three">
                        <div class="row clearfix">
                            <!--Image Column-->
                            <div class="image-column col-md-4 col-sm-4 col-xs-12">
                                <div class="image">
                                    <a href="{{ url('/blog/'.$data->slug)}}"><img src="{{$data->image}}" alt=""></a>
                                </div>
                            </div>
                            <!--Content Column-->
                            <div class="content-column col-md-8 col-sm-8 col-xs-12">
                                <div class="content-inner">
                                    <h3><a href="{{ url('/blog/'.$data->slug)}}">{{$data->title}}</a></h3>
                                    <ul class="post-meta">
                                        <li><a><span class="icon fa fa-user"></span>{{$data->author_name}}</a></li>
                                    </ul>
                                </div>
                                <div class="text">{{$data->summary}}</div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <!--Tab Column-->
               
               @endforeach      
                    
            </div>
           
            </div>
        </div>
    </section>
</section>
    <!--End Blog Section-->
        
@endsection
