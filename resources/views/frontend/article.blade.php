@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')


    <!--Page Title-->
    <section class="page-title" style="background-image:url({{ url('frontend/assets/')}}/images/background/5.jpg);">
        <div class="auto-container">
            <div class="inner-box">
                <h1>blog Details</h1>
                <ul class="bread-crumb">
                    <li><a href="index.html">Home</a></li>
                    <li>Blog</li>
                    <li>blog Details</li>
                </ul>
            </div>
        </div>
    </section>
    <!--End Page Title-->
    
    <!--Sidebar Page-->
    <div class="sidebar-page-container">
        <div class="auto-container">
            <div class="row clearfix">
                
                <!--Content Side-->
                <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <!--Blog-->
                    <section class="blog-single">
                        
                        <!--News Style Four-->
                        <div class="news-style-four">
                            <div class="inner-box">
                                <!--Image Column-->
                                <div class="image">
                                    <img src="{{$blogsdata->image}}" alt="">
                                </div>
                                <!--Content Column-->
                                <div class="content-column">
                                    <div class="inner">
                                        <div class="post-date">{{$blogsdata->updated_at}}</div>
                                        <h3>{{$blogsdata->title}}</h3>
                                        <ul class="post-meta">
                                            <li>by <span>{{$blogsdata->author_name}}</span></li>
                                            <!-- <li><a href="blog-single.html"><span class="icon fa fa-commenting-o"></span> 7 Comments</a></li>    
                                            <li><a href="blog-single.html"><span class="icon fa fa-eye"></span>18 Views</a></li> -->
                                        </ul>
                                        <div class="text">
                                            <p>{!!$blogsdata->content!!}</p>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <!--Comments Area-->
                        <!-- <div class="comments-area">
                            <div class="group-title"><h2>Comments (3)</h2></div>
                           
                            <div class="comment-box">
                                <div class="comment">
                                    <div class="author-thumb">
                                        <div class="image">
                                            <img src="{{ url('frontend/assets/')}}/images/resource/author-4.jpg" alt="">
                                        </div>
                                        <div class="author-name">Game Smith</div>
                                    </div>
                                    <div class="comment-inner">
                                        <div class="comment-info"><div class="comment-time">Mar 7, 2017</div></div>
                                        <div class="text">leads a rag-tag fugitive fleet on a lonely quest - a shining planet known as Earth. So lets make the most of this beautiful day. </div>
                                    </div>
                                </div>
                                <a class="comment-reply" href="#"><span class="icon fa fa-mail-reply-all"></span> Reply</a>
                            </div>
                            
                            <div class="comment-box reply-comment">
                                <div class="comment">
                                    <div class="author-thumb">
                                        <div class="image">
                                            <img src="{{ url('frontend/assets/')}}/images/resource/author-5.jpg" alt="">
                                        </div>
                                        <div class="author-name">Adam Gem</div>
                                    </div>
                                    <div class="comment-inner">
                                        <div class="comment-info clearfix"><div class="comment-time">Mar 10, 2017</div></div>
                                        <div class="text">Just two good ol' boys Wouldn't change if they could. Fightin' the system like a true modern day Robin Hood.</div>
                                    </div>
                                </div>
                                <a class="comment-reply" href="#"><span class="icon fa fa-mail-reply-all"></span> Reply</a>
                            </div>
                         
                            <div class="comment-box">
                                <div class="comment">
                                    <div class="author-thumb">
                                        <div class="image">
                                            <img src="{{ url('frontend/assets/')}}/images/resource/author-6.jpg" alt="">
                                        </div>
                                        <div class="author-name">John Doe</div>
                                    </div>
                                    <div class="comment-inner">
                                        <div class="comment-info clearfix"><div class="comment-time">Mar 13, 2017</div></div>
                                        <div class="text">Ne Go Speed Racer go. And you know where you were then. Mister we could use a man like Herbert Hoover again.</div>
                                    </div>
                                </div>
                                <a class="comment-reply" href="#"><span class="icon fa fa-mail-reply-all"></span> Reply</a>
                            </div>
                        </div>
                     
                        
                        
                        <div class="comment-form">
                            <div class="group-title"><h2>Leave a Comment</h2></div>
                            
                            <form method="post" action="contact.html">
                                <div class="row clearfix">
                                
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                                        <textarea name="message" placeholder="Type Your Message..."></textarea>
                                    </div>
                                
                                    <div class="col-md-4 col-sm-6 col-xs-12 form-group">
                                        <input type="text" name="username" placeholder="Your full name.. " required="">
                                    </div>
    
                                    <div class="col-md-4 col-sm-6 col-xs-12 form-group">
                                        <input type="email" name="email" placeholder="Your email id.. " required="">
                                    </div>
                                    
                                    <div class="col-md-4 col-sm-12 col-xs-12 form-group">
                                        <input type="text" name="website" placeholder="Website" required="">
                                    </div>
    
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                                        <button class="theme-btn btn-style-one" type="submit" name="submit-form">Send Your Message</button>
                                    </div>
    
                                </div>
                            </form>
    
                        </div> -->
                 
                        
                    </section>
                    
                </div>
                
                <!--Sidebar-->
                <div class="sidebar-side col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <aside class="sidebar">
                        
                        
                        <!--Services Post Widget-->
                        <div class="sidebar-widget popular-posts">
                            <div class="sidebar-title">
                                <h3>Latest Post</h3>
                            </div>
                            <!--Post-->
                            @foreach($alldata as $latestpost)
                            <article class="post">
                                <figure class="post-thumb img-circle"><a href="{{url('blog/'.$latestpost->slug)}}"><img src="{{$latestpost->image}}" alt=""></a></figure>
                                <div class="text"><a href="{{url('blog/'.$latestpost->slug)}}">{{$latestpost->title}}</a></div>
                                <div class="post-info">Posted by {{$latestpost->author_name}}</div>
                            </article>
                            @endforeach
                            <!--Post-->
                        </div>
                        
                        
                    </aside>
                </div>
                
            </div>
        </div>
    </div>


@endsection