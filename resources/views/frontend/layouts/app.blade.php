@langrtl
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="rtl">
@else
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@endlangrtl
<head>
 {!! SEOMeta::generate() !!}
    {!! OpenGraph::generate() !!}
    {!! Twitter::generate() !!}
    {!! JsonLd::generate() !!}
<meta charset="utf-8">
<meta name="description" content="@yield('meta_description', 'Rumah Sunat Bali')">
<meta name="author" content="@yield('meta_author', 'GeneralWeb')">
<!-- Stylesheets -->
<link href="{{ url('frontend/assets/')}}/css/bootstrap.css" rel="stylesheet">
<link href="{{ url('frontend/assets/')}}/css/revolution-slider.css" rel="stylesheet">
<link href="{{ url('frontend/assets/')}}/css/style.css" rel="stylesheet">
<link href="{{ url('frontend/assets/')}}/css/jquery-ui.css" rel="stylesheet">

<!--Favicon-->
<link rel="shortcut icon" href="{{ url('frontend/assets/')}}/images/faviconrumahsunat.ico" type="image/x-icon">
<link rel="icon" href="{{ url('frontend/assets/')}}/images/faviconrumahsunat.ico" type="image/x-icon">
<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<link href="{{ url('frontend/assets/')}}/css/responsive.css" rel="stylesheet">
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="{{ url('frontend/assets/')}}/js/respond.js"></script><![endif]-->
</head>

<body>
<div class="page-wrapper">
    
    <!-- Preloader -->
    <div class="preloader"></div>
    
    <!-- Main Header-->
    <header class="main-header">
        
        <!-- Header Top -->
        <div class="header-top">
            <div class="auto-container clearfix">
                <!--Top Left-->
                <div class="top-left pull-left">
                    <ul class="links-nav clearfix">
                        <li><span class="icon fa fa-envelope-o"></span><a href="#">info@rumahsunatbali.com</a></li>
                        <li><span class="icon fa fa-phone"></span><a href="#">Call Us Now : (0877) 7711 4800</a></li>
                    </ul>
                </div>
                
                <!--Top Right-->
                <div class="top-right pull-right">
                    <div class="top-left pull-left">
                    <ul class="links-nav clearfix">
                        <li><span class="icon fa fa-clock-o"></span><a href="#">WE ARE OPEN! : Everyday 9:00-20:00</a></li>
                    </ul>
                </div>
                </div>
            </div>
        </div>
        <!-- Header Top End -->
        
        <!--Header-Upper-->
        <div class="header-upper">
            <div class="auto-container">
                    
                <div class="logo-outer">
                    <div class="logo"><a href="{{url('/')}}"><img src="{{ url('frontend/assets/')}}/images/logo.png" alt="" title=""></a></div>
                </div>
                    
                <div class="nav-outer clearfix">
                
                    <!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-header">
                            <!-- Toggle Button -->      
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            </button>
                        </div>
                        
                        <div class="navbar-collapse collapse clearfix">
                            <!--Left Nav-->
                            <ul class="navigation left-nav clearfix">
                                <li class="{{ Request::is('/')? 'current':'' }}"><a href="{{ url('/')}}">Home</a>
                                    <!-- <ul>
                                        <li><a href="index.html">Homepage One</a></li>
                                        <li><a href="index-2.html">Homepage Two</a></li>
                                        <li class="dropdown"><a href="#">Header Styles</a>
                                            <ul>
                                                <li><a href="index.html">Header Style One</a></li>
                                                <li><a href="index-2.html">Header Style Two</a></li>
                                            </ul>
                                        </li>
                                    </ul> -->
                                </li>
                                <li class="{{ Request::is('/#metode')? 'current':'' }}"><a href="{{ url('/')}}/#metode">Metode</a></li>
                                <li class="{{ Request::is('/#layanan')? 'current':'' }}"><a href="{{ url('/')}}/#layanan">Layanan</a>
                                    <!-- <ul>
                                        <li><a href="classes.html">Classes</a></li>
                                        <li><a href="classes-single.html">Alphabet matching</a></li>
                                        <li><a href="classes-single.html">Letter matching</a></li>
                                        <li><a href="classes-single.html">Color matching</a></li>
                                        <li><a href="classes-single.html">Little Stars</a></li>
                                        <li><a href="classes-single.html">Smart Turtles</a></li>
                                    </ul> -->
                                </li>
                                
                                    <!-- <ul>
                                        <li><a href="teachers.html">Teachers</a></li>
                                        <li><a href="features.html">Features</a></li>
                                        <li><a href="error-page.html">404 Page</a></li>
                                    </ul> -->
                                </li>
                            </ul>
                            <!--Right Nav-->
                            <ul class="navigation right-nav clearfix">
                                <li class="{{ Request::is('blogs')? 'current':'' }}"><a href="{{ url('blogs')}}">Blog</a></li>
                                <li class="{{ Request::is('paketharga')? 'current':'' }}"><a href="{{ url('paketharga')}}">Paket Harga</a>
                                    <!-- <ul>
                                        <li><a href="blog.html">Our Blog</a></li>
                                        <li><a href="blog-single.html">Blog Single</a></li>
                                    </ul>
                                </li> -->
                                <li class="{{ Request::is('contact')? 'current':'' }}"><a href="{{ url('contact')}}">Kontak</a></li>
                            </ul>
                        </div>
                    </nav>
                    
                    <!--Search Box-->
                    <!-- <div class="search-box-outer">
                        <div class="dropdown">
                            <button class="search-box-btn dropdown-toggle" type="button" id="dropdownMenu3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="fa fa-search"></span></button>
                            <ul class="dropdown-menu pull-right search-panel" aria-labelledby="dropdownMenu3">
                                <li class="panel-outer">
                                    <div class="form-container">
                                        <form method="post" action="blog.html">
                                            <div class="form-group">
                                                <input type="search" name="field-name" value="" placeholder="Search Here" required="">
                                                <button type="submit" class="search-btn"><span class="fa fa-search"></span></button>
                                            </div>
                                        </form>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div> -->
                    
                </div>
            </div>
        </div>
        <!--End Header Upper-->
        
        <!--Sticky Header-->
        <div class="sticky-header">
            <div class="auto-container clearfix">
                <!--Logo-->
                <div class="logo pull-left">
                    <a href="{{url('/')}}" class="img-responsive"><img src="{{ url('frontend/assets/')}}/images/logo-small.png" alt="" title=""></a>
                </div>
                
                <!--Right Col-->
                <div class="right-col pull-right">
                    <!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-header">
                            <!-- Toggle Button -->      
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        
                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <li class="{{ Request::is('/')? 'current':'' }}"><a href="{{url('/')}}">Home</a>
                                    <!-- <ul>
                                        <li><a href="index.html">Homepage One</a></li>
                                        <li><a href="index-2.html">Homepage Two</a></li>
                                        <li class="dropdown"><a href="#">Header Styles</a>
                                            <ul>
                                                <li><a href="index.html">Header Style One</a></li>
                                                <li><a href="index-2.html">Header Style Two</a></li>
                                            </ul>
                                        </li>
                                    </ul> -->
                                </li>
                                <li class="{{ Request::is ('/#metode')?  'current':'' }}"><a href="{{ url('/')}}/#metode">Metode</a></li>
                                <li class="{{ Request::is ('/$layanan')?  'current':'' }}"><a href="{{ url('/')}}/#layanan">Layanan</a>
                                   <!--  <ul>
                                        <li><a href="classes.html">Classes</a></li>
                                        <li><a href="classes-single.html">Alphabet matching</a></li>
                                        <li><a href="classes-single.html">Letter matching</a></li>
                                        <li><a href="classes-single.html">Color matching</a></li>
                                        <li><a href="classes-single.html">Little Stars</a></li>
                                        <li><a href="classes-single.html">Smart Turtles</a></li>
                                    </ul> -->
                                </li>
                               
                                   <!--  <ul>
                                        <li><a href="teachers.html">Teachers</a></li>
                                        <li><a href="features.html">Features</a></li>
                                        <li><a href="error-page.html">404 Page</a></li>
                                    </ul> -->
                                </li>
                                <li class="{{ Request::is('blogs')? 'current':'' }}"><a href="{{ url('blogs')}}">Blog</a></li>
                                 <li class="{{ Request::is('paketharga')? 'current':'' }}"><a href="{{ url('paketharga')}}">Paket Harga</a>
                                   <!--  <ul>
                                        <li><a href="blog.html">Our Blog</a></li>
                                        <li><a href="blog-single.html">Blog Single</a></li>
                                    </ul> -->
                                </li>
                                <li class="{{ Request::is('contact')? 'current':'' }}"><a href="{{ url('contact')}}">Kontak</a></li>
                            </ul>
                        </div>
                    </nav><!-- Main Menu End-->
                </div>
                
            </div>
        </div>
        <!--End Sticky Header-->
    
    </header>
    <!--End Main Header -->
    
    <div id="appVue">
        @yield('content')
    </div> 

    <!--Involved Section-->
    <section class="involved-section">
        <div class="auto-container">
            <div class="involved-inner wow fadeInUp" style="background-image:url({{ url('frontend/assets/')}}/images/background/pattern-3.png);">
                <div class="row clearfix">
                    <div class="column col-md-8 col-sm-12 col-xs-12">
                        <h2>Yuk! cari tahu tentang Rumah Sunat Bali</h2>
                    </div>
                    <div class="btn-column col-md-4 col-sm-12 col-xs-12">
                        <a href="{{ url('contact')}}" class="theme-btn btn-style-one">Get Involved</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--End Involved Section-->

    <!--Main Footer-->
    <footer class="main-footer" style="background-image:url({{ url('frontend/assets/')}}/images/background/pattern-2.png);">
        <div class="auto-container">
            <!--widgets section-->
            <div class="widgets-section">
                <div class="row clearfix">
                
                    <!--Big Column-->
                    <div class="big-column col-md-6 col-sm-12 col-xs-12">
                        <div class="row clearfix">
                            <!--Footer Column-->
                            <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                                <div class="footer-widget logo-widget">
                                    <div class="footer-logo">
                                        <a href="{{ url('/')}}"><img src="{{ url('frontend/assets/')}}/images/logo-2.png" alt=""></a>
                                    </div>
                                    <div class="widget-content">
                                        <div class="text">Rumah Sunat Bali memberikan pengalaman professional dalam menjalankan proses Sirkumsisi/Sunat dengan layanan dan metode yang sesuai dengan prosedur tindakan medis</div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <!--Footer Column-->
                            <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                                <!--Links Widget-->
                                <div class="footer-widget links-widget">
                                    <div class="footer-title">
                                        <h2>Layanan Kami</h2>
                                    </div>
                                    <div class="widget-content">
                                        <ul class="list">
                                            <li><a href="{{ url('/')}}/#layanan">Sunat Balita</a></li>
                                            <li><a href="{{ url('/')}}/#layanan">Sunat Anak</a></li>
                                            <li><a href="{{ url('/')}}/#layanan">Sunat Remaja</a></li>
                                            <li><a href="{{ url('/')}}/#layanan">Sunat Dewasa</a></li>
                                            <li><a href="{{ url('/')}}/#layanan">Sunat Gemuk</a></li>
                                            <li><a href="{{ url('/')}}/#layanan">Sunat Kondisi Khusus</a></li>
                                            <li><a href="{{ url('/')}}/#layanan">Home Care Service</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    
                    <!--Big Column-->
                    <div class="big-column col-md-6 col-sm-12 col-xs-12">
                        <div class="row clearfix">
                        
                            <!--Footer Column-->
                            <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                                <!--Links Widget-->
                                <div class="footer-widget links-widget">
                                    <div class="footer-title">
                                        <h2>Quick Kontak</h2>
                                    </div>
                                    <div class="widget-content">
                                        <ul class="list">
                                            <li>(0877) 7711 4800</li>
                                            <li>info@rumahsunatbali.com</li>
                                            <li>Denpasar, Bali</li>
                                        </ul>
                                        <br>
                                        <ul class="social-links-two">
                                            <li class="facebook"><a href="https://www.facebook.com/sunatbali"><span class="fa fa-facebook"></span></a></li>
                                            <li class="google-plus"><a href="https://www.instagram.com/rumahsunatbali/"><span class="fa fa-instagram"></span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            
                            <!--Footer Column-->
                            <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                                
                                <div class="footer-widget subscribe-widget">
                                    <div class="footer-title">
                                        <h2>News letter</h2>
                                    </div>
                                    <div class="widget-content">
                                        <div class="newsletter-form">
                                            <form method="post" action="contact.html">
                                                <div class="form-group">
                                                    <input type="text" name="name" value="" placeholder="Name *" required="">
                                                </div>
                                                <div class="form-group">
                                                    <input type="email" name="email" value="" placeholder="Email Id" required="">
                                                </div>
                                                <div class="form-group">
                                                    <button type="submit" class="theme-btn btn-style-one">SUBSCRIBE</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            
                        </div>
                    </div>
                    
                </div>
            </div>
            
            <!--Footer Bottom-->
            <div class="footer-bottom">
                <div class="row clearfix">
                    <div class="column col-md-6 col-sm-6 col-xs-12">
                        <div class="copyright">&copy; Copyrights 2020 Rumah Sunat Bali. All Rights Reserved</div>
                    </div>
                    
                </div>
            </div>
            
        </div>
    </footer>
    
</div>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target=".main-header"><span class="icon fa fa-long-arrow-up"></span></div>

<script src="{{ url('frontend/assets/')}}/js/jquery.js"></script>
<script src="{{ url('frontend/assets/')}}/js/bootstrap.min.js"></script>
<script src="{{ url('frontend/assets/')}}/js/revolution.min.js"></script>
<script src="{{ url('frontend/assets/')}}/js/jquery.fancybox.pack.js"></script>
<script src="{{ url('frontend/assets/')}}/js/jquery.fancybox-media.js"></script>
<script src="{{ url('frontend/assets/')}}/js/owl.js"></script>
<script src="{{ url('frontend/assets/')}}/js/appear.js"></script>
<script src="{{ url('frontend/assets/')}}/js/jquery-ui.js"></script>
<script src="{{ url('frontend/assets/')}}/js/wow.js"></script>
<script src="{{ url('frontend/assets/')}}/js/script.js"></script>

        <!-- Scripts -->
    <!-- @stack('before-scripts')
        {!! script(mix('js/manifest.js')) !!}
        {!! script(mix('js/vendor.js')) !!} -->
    @stack('after-scripts')
    @yield('pagespecificscripts')
</body>
</html>